# About

A [Docker Compose](https://docs.docker.com/compose/) example which runs [Lita](https://www.lita.io/) chat bot.